const builtin = @import("builtin");
const std = @import("std");
const fs = std.fs;
const io = std.io;

pub fn main() !void {
    const std_out = std.io.getStdOut().writer();
    var buffered = std.io.bufferedWriter(std_out);
    var writer = buffered.writer();

    const args = std.os.argv;

    if (args.len < 2) {
        const stdin = std.io.getStdIn();
        var reader = stdin.reader();
        var buf: [1024]u8 = undefined;
        while (try reader.readUntilDelimiterOrEof(&buf, '\n')) |arg|
            try processArg(&writer, arg);
    } else {
        for (args[1..]) |arg|
            try processArg(&writer, std.mem.span(arg));
    }

    try buffered.flush();
}

fn processArg(writer: anytype, arg: []const u8) !void {
    try transliterate(writer, arg);
    try writer.writeByte('\n');
}

const unicode = std.unicode;

fn transliterate(writer: anytype, str: []const u8) !void {
    const view = try unicode.Utf8View.init(str);
    var it = view.iterator();

    while (it.nextCodepoint()) |cp| {
        var out_cp = cp;
        var utf8: [4]u8 = undefined;
        const cp_upper = toLower(cp);

        inline for (mapping) |pair| {
            if (pair[1] == cp_upper) {
                out_cp = pair[0];
                break;
            } else if (pair[0] == cp) {
                out_cp = pair[1];
                break;
            }
        }

        const l = try unicode.utf8Encode(out_cp, &utf8);
        try writer.writeAll(utf8[0..l]);
    }
}

fn toLower(cp: u21) u21 {
    const case_mask: u8 = 0x20;

    return switch (cp) {
        0x0041...0x005A,
        0x0061...0x007A,
        0x00C0...0x00D6,
        0x00D8...0x00F6,
        0x00F8...0x00FF,
        0x16A0...0x16FF,
        => cp | case_mask,
        else => cp,
    };
}

fn printUsage(writer: anytype) !void {
    try writer.writeAll("usage: umstafa [input]\n");
}

const mapping = [_][2]u21{
    .{ 'ᛆ', 'a' },
    .{ 'ᛒ', 'b' },
    .{ 'ᛍ', 'c' },
    .{ 'ᛑ', 'd' },
    .{ 'ᛂ', 'e' },
    .{ 'ᚠ', 'f' },
    .{ 'ᚵ', 'g' },
    .{ 'ᚼ', 'h' },
    .{ 'ᛁ', 'i' },
    .{ 'ᛃ', 'j' },
    .{ 'ᚴ', 'k' },
    .{ 'ᛚ', 'l' },
    .{ 'ᛘ', 'm' },
    .{ 'ᚿ', 'n' },
    .{ 'ᚮ', 'o' },
    .{ 'ᛔ', 'p' },
    .{ 'ᛩ', 'q' },
    .{ 'ᚱ', 'r' },
    .{ 'ᛋ', 's' },
    .{ 'ᛐ', 't' },
    .{ 'ᚢ', 'u' },
    .{ 'ᚡ', 'v' },
    .{ 'ᚥ', 'w' },
    .{ 'ᛪ', 'x' },
    .{ 'ᛦ', 'y' },
    .{ 'ᛎ', 'z' },
    .{ 'ᚨ', 'á' },
    //    .{ 'ᛅ', 'Ä' },
    //    .{ 'ᚨ', 'Å' }, // ???
    .{ 'ᛅ', 'æ' },
    .{ 'ᛖ', 'é' },
    .{ 'ᛇ', 'í' },
    .{ 'ᚧ', 'ð' },
    .{ 'ᛟ', 'ó' },
    .{ 'ᚯ', 'ö' },
    .{ 'ᚤ', 'ú' },
    .{ 'ᛨ', 'ý' },
    .{ 'ᚦ', 'þ' },
    .{ '᛫', ' ' },
    .{ '᛭', ' ' },
    .{ '᛬', ' ' },
};

const expectEqual = std.testing.expectEqual;

test "toUpper" {
    try expectEqual(@as(u21, 'a'), toLower('A'));
    try expectEqual(@as(u21, 'a'), toLower('a'));
    try expectEqual(@as(u21, 'á'), toLower('á'));
    try expectEqual(@as(u21, 'æ'), toLower('Æ'));
}

const test_latin =
    \\Áin í hrauninu
    \\Í bláum draumi
    \\Hún unir ein
    \\Með ærslum leikur
    \\Á strengi og flúðir
    \\Og glettin skvettir
    \\Á gráan stein
    \\Í hyl og lygnu
    \\Er hægt á ferð
    \\Með hæverskum þokka
    \\Áin niðar
    \\Sí-endurfædd
    \\Og undraverð
    \\Hún fremur þá list
    \\Sem fegurst er
    \\Úr fornum eldi
    \\Er hljómbotn gerður
    \\Ég heyri óminn
    \\Í hjarta mér
    \\Ég heyri óminn
    \\Í hjarta mér
;

const test_runes =
    \\ᚨᛁᚿ᛫ᛇ᛫ᚼᚱᛆᚢᚿᛁᚿᚢ
    \\ᛇ᛫ᛒᛚᚨᚢᛘ᛫ᛑᚱᛆᚢᛘᛁ
    \\ᚼᚤᚿ᛫ᚢᚿᛁᚱ᛫ᛂᛁᚿ
    \\ᛘᛂᚧ᛫ᛅᚱᛋᛚᚢᛘ᛫ᛚᛂᛁᚴᚢᚱ
    \\ᚨ᛫ᛋᛐᚱᛂᚿᚵᛁ᛫ᚮᚵ᛫ᚠᛚᚤᚧᛁᚱ
    \\ᚮᚵ᛫ᚵᛚᛂᛐᛐᛁᚿ᛫ᛋᚴᚡᛂᛐᛐᛁᚱ
    \\ᚨ᛫ᚵᚱᚨᛆᚿ᛫ᛋᛐᛂᛁᚿ
    \\ᛇ᛫ᚼᛦᛚ᛫ᚮᚵ᛫ᛚᛦᚵᚿᚢ
    \\ᛂᚱ᛫ᚼᛅᚵᛐ᛫ᚨ᛫ᚠᛂᚱᚧ
    \\ᛘᛂᚧ᛫ᚼᛅᚡᛂᚱᛋᚴᚢᛘ᛫ᚦᚮᚴᚴᛆ
    \\ᚨᛁᚿ᛫ᚿᛁᚧᛆᚱ
    \\ᛋᛇ-ᛂᚿᛑᚢᚱᚠᛅᛑᛑ
    \\ᚮᚵ᛫ᚢᚿᛑᚱᛆᚡᛂᚱᚧ
    \\ᚼᚤᚿ᛫ᚠᚱᛂᛘᚢᚱ᛫ᚦᚨ᛫ᛚᛁᛋᛐ
    \\ᛋᛂᛘ᛫ᚠᛂᚵᚢᚱᛋᛐ᛫ᛂᚱ
    \\ᚤᚱ᛫ᚠᚮᚱᚿᚢᛘ᛫ᛂᛚᛑᛁ
    \\ᛂᚱ᛫ᚼᛚᛃᛟᛘᛒᚮᛐᚿ᛫ᚵᛂᚱᚧᚢᚱ
    \\ᛖᚵ᛫ᚼᛂᛦᚱᛁ᛫ᛟᛘᛁᚿᚿ
    \\ᛇ᛫ᚼᛃᛆᚱᛐᛆ᛫ᛘᛖᚱ
    \\ᛖᚵ᛫ᚼᛂᛦᚱᛁ᛫ᛟᛘᛁᚿᚿ
    \\ᛇ᛫ᚼᛃᛆᚱᛐᛆ᛫ᛘᛖᚱ
;

test "runic output is as expected" {
    var out_buf: [test_runes.len * 2]u8 = undefined;
    var out = io.fixedBufferStream(&out_buf);

    try transliterate(&out.writer(), test_latin);
    try std.testing.expectEqualSlices(u8, test_runes, out.getWritten());
}

test "roundtrip" {
    var out_buf: [64]u8 = undefined;
    var out = io.fixedBufferStream(&out_buf);
    var out2_buf: [64]u8 = undefined;
    var out2 = io.fixedBufferStream(&out2_buf);
    const in = "komdu sæl";

    try transliterate(&out.writer(), in);
    try transliterate(&out2.writer(), out.getWritten());
    try std.testing.expectEqualSlices(u8, in, out2.getWritten());
}
