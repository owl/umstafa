A program that transliterates modern Icelandic to/from runes.

It is based on medieval runes, with vowels from the Elder Fuþark used for the
letters á, é, í, ó, ú, and ý.

Send any patches/corrections to [ugla@u8.is](mailto:ugla@u8.is).

Build it with [Zig](https://ziglang.org/) like this:

```shell
zig build -Doptimize=ReleaseSmall --prefix ~/.local # or something else in your $PATH
# run it
umstafa 'sæl öll' # prints ᛋᛅᛚᛚ᛫ᚯᛚᛚ
umstafa 'ᛋᛅᛚᛚ᛫ᚯᛚᛚ' # prints sæll öll
```
