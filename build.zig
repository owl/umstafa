const builtin = @import("builtin");
const std = @import("std");
const name = "umstafa";

pub fn build(b: *std.Build) void {
    const mode = b.standardOptimizeOption(.{});
    const target = b.standardTargetOptions(.{});
    const exe = b.addExecutable(.{
        .name = name,
        .root_source_file = b.path("src" ++ std.fs.path.sep_str ++ "main.zig"),
        .optimize = mode,
        .target = target,
        .single_threaded = true,
        .strip = b.option(bool, "strip", "Strip executable") orelse (mode != .Debug),
    });
    exe.pie = b.option(bool, "pie", "Enable PIE") orelse false;

    b.default_step.dependOn(&exe.step);
    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    const run_step = b.step("run", "Run " ++ name);

    run_step.dependOn(&run_cmd.step);
}
